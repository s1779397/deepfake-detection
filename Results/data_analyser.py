# External Libraries
from telnetlib import AUTHENTICATION
import matplotlib.pyplot as plt
import numpy as np
import sklearn
from sklearn.metrics import roc_curve
from sklearn.metrics import RocCurveDisplay
import scipy
from scipy.optimize import brentq
from scipy.interpolate import interp1d
import time

# Standard Libraries
import os
from os import listdir

# parameters
# its best to copy a single run into a folder, and run from that folder
filename = '\deepfake-detection\Results\\run_15epcoh_60fr_[07-07]\\f2f_true'
dirname =os.getcwd() #os.path.dirname(__file__)

# beun solution for sorting the npy files in the correct way
def calcKey(epoch):
    res = ""
    str = epoch[0][0:-4]
    for c in str:
        if c.isdigit():
            res = res + c
        else:
            res = res + "0"

    key = int(res[0:10] + res[-2] + res[-1])
    print(str)
    print(key)
    return key


def main():
    # load the .npy files of every epoch
    print('filedirectory::: ' + dirname)
    directory_path = dirname + filename
    os.chdir(directory_path)
    print(os.path.abspath("."))

    # load all the .npy files
    file_types = ['npy', 'npz']
    np_vars = {dir_content: np.load(dir_content,allow_pickle=True)
           for dir_content in listdir(directory_path)
           if dir_content.split('.')[-1] in file_types}

    i = 0
    #create empty arrays for plotting
    AUC = []
    EER = []
    EERcheck = []
    EERcheck2 = []
    names = []
    for epoch in sorted(np_vars.items(), key = calcKey):
        
        #distances = epoch
        distances = epoch[1].item()['distances']
        labels    = epoch[1].item()['labels']
        dist_norm = distances / max(distances)

        scores = 1 - dist_norm

        # roc curve
        fpr, tpr, threshold = roc_curve(labels, scores, pos_label=1)   
        roc_display = RocCurveDisplay(fpr=fpr, tpr=tpr).plot()
        #plt.savefig(epoch[0]+'roc.png')
        plt.close('all')

        #area under curve calculation
        AUC.append(sklearn.metrics.auc(fpr, tpr)) #              
        
        #equal error rate
        fnr = 1 - tpr #cakulate false negative rate
        eer_threshold = threshold[np.nanargmin(np.absolute((fnr - fpr)))]   # find threshold at whch
        EER.append(fpr[np.nanargmin(np.absolute((fnr - fpr)))])             # find the EER
        EERcheck.append(fnr[np.nanargmin(np.absolute((fnr - fpr)))])        # find the EER from the fnr, which should be the same
        
        #different method to compare
        #EERcheck2.append(brentq(lambda x : 1. - x - interp1d(fpr, tpr)(x), 0., 1.))
        #thresh = interp1d(fpr, threshold)(EERcheck2)

        #names
        names.append(epoch[0])
        
        #print(names[i],' AUC= ',AUC[i],' EER= ',EER[i],'EERcheck ',EERcheck[i])
        i = i + 1 

    
    plt.close('all')
    plt.plot(AUC[0:-1])
    plt.xlabel('epoch')
    plt.ylabel('AUC')
    plt.title('AUC' + epoch[0])
    plt.savefig(epoch[0]+'AUC.png')
    #plt.show()
    plt.close('all')

    plt.plot(EER[0:-1])
    plt.plot(EERcheck[0:-1])
    #plt.plot(EERcheck2[i-num_epochs:i])
    plt.xlabel('epoch')
    plt.ylabel('EER')
    plt.title('EER' + epoch[0])
    plt.savefig(epoch[0]+'EER.png')
    plt.legend(['fpr', 'fnr'])
    #plt.show()
    plt.close('all')
    

main()