# importing libraries
import cv2
import math
from os import listdir
from os.path import isfile, join

# Video width
# width = 350  # nice width for multiple videos next to each other
# width = 800  # nice width for a single video
width = 500

# FPS downsampling rate, 3 would mean that 1 out of 3 frames is displayed
downsamplingRate = 1

# Flag to show videos sequential instead of all at the same time
sequential = False
# sequential = True

# Create list for videoCapture objects
caps = []

vid1 = '154'
vid2 = '386'

fake1 = vid1 + '_' + vid2
fake2 = vid2 + '_' + vid1

# Create list for video's to open
files = ['..//dataset//original_sequences//youtube//c23//videos//' + vid1 + '.mp4',
         '..//dataset//original_sequences//youtube//c23//videos//' + vid2 + '.mp4',

         '..//dataset//manipulated_sequences//Face2Face//c23//videos//' + fake2 + '.mp4',
         '..//dataset//manipulated_sequences//NeuralTextures//c23//videos//' + fake2 + '.mp4',


         '..//dataset//manipulated_sequences//Face2Face//c23//videos//' + fake1 + '.mp4',
         '..//dataset//manipulated_sequences//NeuralTextures//c23//videos//' + fake1 + '.mp4'
         ]

# All original video's
# dir_orig = '..//dataset//original_sequences//youtube//c23//videos//'
# files = [join(dir_orig, f) for f in listdir(dir_orig) if isfile(join(dir_orig, f))]

# Create a VideoCapture object and read from input file
for file in files:
    caps.append(cv2.VideoCapture(file))

    if not sequential:
        # tile windows
        vids_per_row = 2
        # heightstep = width
        heightstep = 340
        cv2.namedWindow(file)
        cv2.moveWindow(file, (width * (files.index(file) % vids_per_row)), (heightstep * math.floor(files.index(file) / vids_per_row)))

if not sequential:
    # Boolean to keep track of EOFs
    done = False
    for cap in caps:
        # Check if camera opened successfully
        if not cap.isOpened():
            print("Error, can't open file: ", files[caps.index(cap)])
            done = True

    # Read until (one of the) videos is completed
    while not done:
        # Press Q on keyboard to Exit
        # Press P on keyboard to Pauze
        key = cv2.waitKey(1)
        if key == ord('q'):
            break
        if key == ord('p'):
            cv2.waitKey(-1)  # wait until any key is pressed

        for cap in caps:
            # Capture frame-by-frame
            ret, frame = cap.read()
            if ret:
                height_original, width_original = frame.shape[:2]

                # calculate the new height based on set width
                height = int(height_original * width / width_original)

                # dsize
                dsize = (width, height)

                # Display the resulting frame
                cv2.imshow(files[caps.index(cap)], cv2.resize(frame, dsize))

            # Break the loop
            else:
                done = True

            for i in range(downsamplingRate - 1):
                # Read extra frames to implement downsampling
                ret, frame = cap.read()

else:  # Display videos sequential
    cv2.namedWindow('Video')
    cv2.moveWindow('Video', 50, 50)

    for cap in caps:
        print(files[caps.index(cap)])
        # Boolean to keep track of EOFs
        done = False

        if not cap.isOpened():
            print("Error, can't open file: ", files[caps.index(cap)])
            done = True

        while not done:
            # Press Q on keyboard to exit
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

            # Capture frame-by-frame
            ret, frame = cap.read()
            if ret:
                height_original, width_original = frame.shape[:2]

                # calculate the new dimensions
                height = int(height_original * width / width_original)

                # dsize
                dsize = (width, height)

                # Display the resulting frame
                cv2.imshow('Video', cv2.resize(frame, dsize))

            # Break the loop
            else:
                done = True

            for i in range(downsamplingRate - 1):
                # Read extra frames to implement downsampling
                ret, frame = cap.read()

# When everything done, release
# the video capture objects
for cap in caps:
    cap.release()

# Closes all the frames
cv2.destroyAllWindows()
