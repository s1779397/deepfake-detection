# External Libraries
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn
import torch
from sklearn.metrics import roc_curve
from sklearn.metrics import RocCurveDisplay
import time

# Standard Libraries
import os

# Modules
from model.ml.lstm import lstmESR
from model.utils import video_loader

EPOCHS = 15
BRANCH = 1
FRAMES = 20


def main():
    # Running device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    # device = "cpu"
    print("device is", device)

    # init model
    net = lstmESR(device)

    print("net initialized")

    # freeze esr9 base and branch layers
    for param in net.esr.base.parameters():
        param.requires_grad = False
    for i in range(9):
        for param in net.esr.convolutional_branches[i].parameters():
            param.requires_grad = False

    print("base and branch layers frozen")

    # enable other lstm and fc layers -- fc layers not used now
    # for param in net.fc.parameters():
    #     param.requires_grad = True
    for param in net.lstm.parameters():
        param.requires_grad = True

    print("lstm parameters enabled")

    # Set optimizer for enabled layers
    optimizer = optim.SGD(filter(lambda p: p.requires_grad, net.parameters()), lr=0.01, momentum=0.9)
    triplet_loss = nn.TripletMarginLoss(margin=1.0, p=2)

    print("optimizers set")

    for epoch in range(EPOCHS):
        # train_data = triplet_loader.get_data()
        if epoch > 5:
            anchor_positive_fraction = 0
            other_negative_fraction = 0
        else:
            # Scale fractions from 0.5 to 0 based on the epoch number
            EPOCH_5 = 5
            anchor_positive_fraction = ((EPOCH_5 - epoch) / EPOCH_5) * 0.5
            other_negative_fraction = ((EPOCH_5 - epoch) / EPOCH_5) * 0.5

        train_data = video_loader.VideoLoader(train=True,
                                              # seed=epoch + 1,  # Use epoch as seed number
                                              nr_frames=FRAMES,
                                              # hard_triplets=True,
                                              anchor_positive_fraction=anchor_positive_fraction,
                                              other_negative_fraction=other_negative_fraction
                                              )
        train_loader = DataLoader(train_data, batch_size=4)

        running_loss = 0.0
        running_updates = 0

        start = None
        for anchor, pos, neg in train_loader:

            if anchor.ndim < 3 or pos.ndim < 3 or neg.ndim < 3:
                continue
            # if start is not None:
            #     end = time.time()
            #     print("Load time: ", end - start)
            #     return
            # print(anchor.shape)

            # Get the inputs
            # print(anchor.shape)
            # start = time.time()
            anchor, pos, neg = anchor.to(device), pos.to(device), neg.to(device)
            # end = time.time()
            # print("To device time:", end-start)

            # Zero the parameter gradients
            # start = time.time()
            optimizer.zero_grad()
            # end = time.time()
            # print("zero grad time:", end - start)

            # print(anchor.size())
            # Forward
            # start = time.time()
            output_a, output_p, output_n = net(anchor, BRANCH).squeeze()[-1], \
                                           net(pos, BRANCH).squeeze()[-1], \
                                           net(neg, BRANCH).squeeze()[-1]
            # end = time.time()
            # print("Inference time: ", end - start)

            # start = time.time()
            dist_pos = torch.sum(((output_a - output_p) ** 2))
            dist_neg = torch.sum(((output_a - output_n) ** 2))
            # end = time.time()
            # print("Distance calc time: ", end - start)

            # start = time.time()
            print("distance a-p:", dist_pos)
            print("distance a-n:", dist_neg)
            # end = time.time()
            # print("Print dist time: ", end - start)

            # Compute loss
            # start = time.time()
            loss = triplet_loss(output_a.unsqueeze(0), output_p.unsqueeze(0), output_n.unsqueeze(0))
            # end = time.time()
            # print("Loss time: ", end - start)

            # Backward
            # start = time.time()
            loss.backward()
            # end = time.time()
            # print("Backwards time: ", end - start)

            # Optimize
            # start = time.time()
            optimizer.step()
            # end = time.time()
            # print("Opt step time: ", end - start)

            # Save loss
            # start = time.time()
            running_loss += loss.item()
            running_updates += 1
            print('Loss this iteration:', loss.item())
            # end = time.time()
            # print("Run loss time: ", end - start)
            # start = time.time()

        # Statistics
        print('[Epochs {:d}--{:d}] Loss: {:.4f}'.format(epoch + 1,
                                                        EPOCHS,
                                                        running_loss / running_updates))
        net.eval()

        for hard_triplets_test in [True, False]:
            test_data = video_loader.VideoLoader(train=False, seed=None,
                                                 nr_frames=FRAMES,
                                                 hard_triplets=hard_triplets_test)  # MAKE SURE TO FETCH TEST DATA
            test_loader = DataLoader(test_data, batch_size=1)

            labels_true = []
            distances = []
            for anchor, pos, neg in test_loader:

                if anchor.ndim < 3 or pos.ndim < 3 or neg.ndim < 3:
                    continue

                anchor, pos, neg = anchor.to(device), pos.to(device), neg.to(device)
                output_a, output_p, output_n = net(anchor, BRANCH).squeeze()[-1], \
                                               net(pos, BRANCH).squeeze()[-1], \
                                               net(neg, BRANCH).squeeze()[-1]

                output_a = output_a.detach().cpu().numpy()
                output_p = output_p.detach().cpu().numpy()
                output_n = output_n.detach().cpu().numpy()

                dist_pos = np.sum(((output_a - output_p) ** 2))
                dist_neg = np.sum(((output_a - output_n) ** 2))

                labels_true.append(1)
                distances.append(dist_pos)
                labels_true.append(0)
                distances.append(dist_neg)
            distances = np.array(distances)
            dist_norm = distances / max(distances)

            results_dict = {'distances': distances,
                            'labels': labels_true}
            np.save("results/20/b4_nt_20fr_hard_" + str(hard_triplets_test) +"_epoch_"+str(epoch), results_dict, allow_pickle=True)

            scores = 1 - dist_norm

            fpr, tpr, threshold = roc_curve(labels_true, scores, pos_label=1)
            print("Threshold =", threshold)
            print("tpr =", tpr)
            print("fpr =", fpr)
            roc_display = RocCurveDisplay(fpr=fpr, tpr=tpr).plot()
            # plt.savefig('roc_epoch_' + str(epoch))
            plt.savefig("results/20/b4_nt_20fr_hard_" + str(hard_triplets_test) +"_epoch_"+str(epoch))

    new_weights = net.state_dict()
    torch.save(new_weights, "model/ml/trained_models/esr_lstm/b4_nt_20fr_esr_lstm.pt")


main()
