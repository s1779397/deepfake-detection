import dlib
import torch

print("dlib num devices:", dlib.cuda.get_num_devices())
print("dlib use cuda:", dlib.DLIB_USE_CUDA)
print("torch cuda avail:", torch.cuda.is_available())