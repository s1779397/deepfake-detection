from controller import cvalidation, cvision
from model.utils import uimage, ufile

input_image_path = "./media/ferrari.PNG"

def main():
    img = uimage.read(input_image_path)
    gpu = False
    cnn_intermediate_features = cvision.get_intermediate_features(img, gpu)
    for branch_features in cnn_intermediate_features:
        branch_features = branch_features.flatten()
        print(branch_features.size())


main()
