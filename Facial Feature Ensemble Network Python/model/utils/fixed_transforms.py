from typing import Union, Tuple
from abc import ABC, abstractmethod

import random
import numpy as np
import torch
from torchvision.transforms import Compose, ColorJitter, Lambda, functional as F, RandomHorizontalFlip, RandomAffine, \
    InterpolationMode
import PIL.Image

# Direct copy of _pil_interpolation_to_str in torchvision.transforms
_pil_interpolation_to_str = {
    PIL.Image.NEAREST: 'PIL.Image.NEAREST',
    PIL.Image.BILINEAR: 'PIL.Image.BILINEAR',
    PIL.Image.BICUBIC: 'PIL.Image.BICUBIC',
    PIL.Image.LANCZOS: 'PIL.Image.LANCZOS',
}


class FixedTransform(ABC):

    def __init__(self):
        self.is_fixed = False

    @abstractmethod
    def fix_transform(self):
        pass


class ColorJitterFixed(ColorJitter, FixedTransform):
    """Randomly change the brightness, contrast and saturation of an image.

    Args:
        brightness (float): How much to jitter brightness. brightness_factor
            is chosen uniformly from [max(0, 1 - brightness), 1 + brightness].
        contrast (float): How much to jitter contrast. contrast_factor
            is chosen uniformly from [max(0, 1 - contrast), 1 + contrast].
        saturation (float): How much to jitter saturation. saturation_factor
            is chosen uniformly from [max(0, 1 - saturation), 1 + saturation].
        hue(float): How much to jitter hue. hue_factor is chosen uniformly from
            [-hue, hue]. Should be >=0 and <= 0.5.
    """

    def __init__(self, brightness: float = 0, contrast: float = 0, saturation: float = 0, hue: float = 0):
        super().__init__(brightness, contrast, saturation, hue)
        self.comp: Union[Compose, None] = None

        self.brightness_fixed: float = 0
        self.contrast_fixed: float = 0
        self.saturation_fixed: float = 0
        self.hue_fixed: float = 0

    def fix_transform(self) -> None:
        transforms = []
        if self.brightness is not None:
            # self.brightness_fixed = random.uniform(max(0, 1 - self.brightness), 1 + self.brightness)
            self.brightness_fixed = random.uniform(self.brightness[0], self.brightness[1])
            transforms.append(Lambda(lambda img: F.adjust_brightness(img, self.brightness_fixed)))

        if self.contrast is not None:
            # self.contrast_fixed = random.uniform(max(0, 1 - self.contrast), 1 + self.contrast)
            self.contrast_fixed = random.uniform(self.contrast[0], self.contrast[1])
            transforms.append(Lambda(lambda img: F.adjust_contrast(img, self.contrast_fixed)))

        if self.saturation is not None:
            # self.saturation_fixed = random.uniform(max(0, 1 - self.saturation), 1 + self.saturation)
            self.saturation_fixed = random.uniform(self.saturation[0], self.saturation[1])
            transforms.append(Lambda(lambda img: F.adjust_saturation(img, self.saturation_fixed)))

        if self.hue is not None:
            # self.hue_fixed = random.uniform(-self.hue, self.hue)
            self.hue_fixed = random.uniform(self.hue[0], self.hue[1])
            transforms.append(Lambda(lambda img: F.adjust_hue(img, self.hue_fixed)))

        random.shuffle(transforms)
        self.comp = Compose(transforms)
        self.is_fixed = True  # Indicate transformation is fixed

    def __call__(self, img):
        """
        Args:
            img (PIL Image): Input image.

        Returns:
            PIL Image: Color jittered image.
        """
        if self.is_fixed:
            return self.comp(img)
        else:
            raise AttributeError("Transformation not fixed yet, call fix_transform() first")

    def __repr__(self):
        format_string = self.__class__.__name__ + '('
        format_string += f'fixed={self.is_fixed}',
        format_string += f', brightness={self.brightness} fixed to {self.brightness_fixed:.4f}'
        format_string += f', contrast={self.contrast} fixed to {self.contrast_fixed:.4f}'
        format_string += f', saturation={self.saturation} fixed to {self.saturation_fixed:.4f}'
        format_string += f', hue={self.hue}) fixed to {self.hue_fixed:.4f}'
        return format_string


class RandomHorizontalFlipFixed(RandomHorizontalFlip, FixedTransform):
    """Horizontally flip the given PIL Image randomly with a given probability.

    Args:
        p (float): probability of the image being flipped. Default value is 0.5
    """

    def __init__(self, p: float = 0.5):
        super().__init__(p)
        self.flip = None

    def fix_transform(self) -> None:
        self.flip = random.random() < self.p
        self.is_fixed = True  # Indicate transformation is fixed

    def __call__(self, img: PIL.Image):
        """
        Args:
            img (PIL Image): Image to be flipped.

        Returns:
            PIL Image: Randomly flipped image.
        """
        if self.is_fixed:
            if self.flip:
                return F.hflip(img)
            return img
        else:
            raise AttributeError("Transformation not fixed yet, call fix_transform() first")

    def __repr__(self) -> str:
        return self.__class__.__name__ + f'(fixed={self.is_fixed}, p={self.p} fixed to {self.flip})'


class RandomAffineFixed(RandomAffine, FixedTransform):

    def __init__(self, degrees, translate=None, scale=None, shear=None, interpolation=InterpolationMode.NEAREST):
        """
        Random affine transformation of the image keeping center invariant

        Args:
            degrees (sequence or number): Range of degrees to select from.
                If degrees is a number instead of sequence like (min, max), the range of degrees
                will be (-degrees, +degrees). Set to 0 to deactivate rotations.
            translate (tuple, optional): tuple of maximum absolute fraction for horizontal
                and vertical translations. For example translate=(a, b), then horizontal shift
                is randomly sampled in the range -img_width * a < dx < img_width * a and vertical shift is
                randomly sampled in the range -img_height * b < dy < img_height * b. Will not translate by default.
            scale (tuple, optional): scaling factor interval, e.g (a, b), then scale is
                randomly sampled from the range a <= scale <= b. Will keep original scale by default.
            shear (sequence or number, optional): Range of degrees to select from.
                If shear is a number, a shear parallel to the x axis in the range (-shear, +shear)
                will be applied. Else if shear is a sequence of 2 values a shear parallel to the x axis in the
                range (shear[0], shear[1]) will be applied. Else if shear is a sequence of 4 values,
                a x-axis shear in (shear[0], shear[1]) and y-axis shear in (shear[2], shear[3]) will be applied.
                Will not apply shear by default.
            interpolation (InterpolationMode): Desired interpolation enum defined by
                :class:`torchvision.transforms.InterpolationMode`. Default is ``InterpolationMode.NEAREST``.
                If input is Tensor, only ``InterpolationMode.NEAREST``, ``InterpolationMode.BILINEAR`` are supported.
                For backward compatibility integer values (e.g. ``PIL.Image.NEAREST``) are still acceptable.
        """
        super().__init__(degrees, translate, scale, shear, interpolation)
        # Initialize all fixed values
        self.angle_fixed: float = 0
        self.translate_fixed: Tuple[int, int] = (0, 0)
        self.scale_fixed: float = 1
        self.shear_fixed: float = 0

    def fix_transform(self) -> None:
        self.angle_fixed = random.uniform(self.degrees[0], self.degrees[1])

        if self.translate is not None:
            self.translate_fixed = (random.uniform(-self.translate[0], self.translate[0]),
                                    random.uniform(-self.translate[1], self.translate[1]))

        if self.scale is not None:
            self.scale_fixed = random.uniform(self.scale[0], self.scale[1])

        if self.shear is not None:
            self.shear_fixed = random.uniform(self.shear[0], self.shear[1])

        self.is_fixed = True  # Indicate transformation is fixed

    def get_fixed_params(self, img_size: Tuple[int, int]) -> Tuple[float, Tuple[int, int], float, float]:
        """Get the fixed parameters used for making the affine transformation based on image size."""
        # First calculate the actual translation in pixels from the fixed random value
        translation: Tuple[int, int] = (np.round(self.translate_fixed[0] * img_size[0]),
                                        np.round(self.translate_fixed[1] * img_size[1]))
        return self.angle_fixed, translation, self.scale_fixed, self.shear_fixed

    def __call__(self, img: PIL.Image) -> torch.Tensor:
        """
            img (PIL Image): Image to be transformed.

        Returns:
            PIL Image: Affine transformed image.
        """
        if self.is_fixed:
            params = self.get_fixed_params(img.size)
            return F.affine(img, *params, interpolation=self.resample, fillcolor=self.fillcolor)
        else:
            raise AttributeError("Transformation not fixed yet, call fix_transform() first")

    def __repr__(self) -> str:
        s = '{name}(fixed={is_fixed}, degrees={degrees} fixed to {angle_fixed:.4f}'
        if self.translate is not None:
            s += ', translate={translate} fixed to {translate_fixed}'
        if self.scale is not None:
            s += ', scale={scale} fixed to {scale_fixed:.4f}'
        if self.shear is not None:
            s += ', shear={shear} fixed to {shear_fixed:.4f}'
        s += ', resample={resample}'
        s += ')'
        d = dict(self.__dict__)
        d['resample'] = _pil_interpolation_to_str[d['resample']]
        return s.format(name=self.__class__.__name__, **d)


if __name__ == '__main__':
    aff1 = RandomAffineFixed(degrees=30,
                             translate=(.1, .1),
                             scale=(1.0, 1.25))
    random.seed(1)
    torch.manual_seed(1)
    aff1.fix_transform()
    print(aff1)

    aff2 = RandomAffineFixed(degrees=30,
                             translate=(.1, .1),
                             scale=(1.0, 1.25))
    random.seed(2)
    torch.manual_seed(2)
    aff2.fix_transform()
    print(aff2)

    aff3 = RandomAffineFixed(degrees=30,
                             translate=(.1, .1),
                             scale=(1.0, 1.25))
    aff3.fix_transform()
    print(aff3)

    color1 = ColorJitterFixed(brightness=0.5,
                              contrast=0.5)
    color2 = ColorJitterFixed(brightness=0.5,
                              contrast=0.5)
    color1.fix_transform()
    color2.fix_transform()
    print(color1)
    print(color2)

    flip1 = RandomHorizontalFlipFixed(p=0.5)
    flip2 = RandomHorizontalFlipFixed(p=0.5)
    flip3 = RandomHorizontalFlipFixed(p=0.5)
    flip4 = RandomHorizontalFlipFixed(p=0.5)
    flip1.fix_transform()
    flip2.fix_transform()
    flip3.fix_transform()
    flip4.fix_transform()
    print(flip1)
    print(flip2)
    print(flip3)
    print(flip4)
