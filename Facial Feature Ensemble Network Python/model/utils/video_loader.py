from pathlib import Path
from typing import Tuple, List, Optional
from math import ceil
import os

import numpy as np
from matplotlib import pyplot as plt
from cv2 import VideoCapture, CAP_PROP_FPS, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT, cvtColor, COLOR_BGR2RGB, \
    CAP_PROP_POS_FRAMES, CAP_PROP_FRAME_COUNT
from torchvision.transforms import Compose, ToPILImage, ToTensor, InterpolationMode
from model.ml.esr_9 import ESR
import torch
import random

from controller.cvision import detect_face, _pre_process_input_image

from model.utils.fixed_transforms import FixedTransform, ColorJitterFixed, RandomHorizontalFlipFixed, RandomAffineFixed

DISABLE_DLIB = False


class Video:
    """VideoCapture wrapper class to hold more information than cv2.VideoCapture"""

    def __init__(self, path: Path, transforms: List[FixedTransform] = None):
        """
        Create new Video object which holds a cv2.VideoCapture and additional information
        :param path: Path to the filename to load
        :param transforms: List of transforms to apply. If not given, some defaults are used.
        """
        self.cvvideo = VideoCapture(str(path))
        self.framerate: float = self.cvvideo.get(CAP_PROP_FPS)
        self.width: int = round(self.cvvideo.get(CAP_PROP_FRAME_WIDTH))
        self.height: int = round(self.cvvideo.get(CAP_PROP_FRAME_HEIGHT))
        self.name: str = path.stem  # Only the filename without extension

        if transforms is None:
            # Deep copy the default list so that the default parameters are not influenced by operations on this class
            self.transforms: List[FixedTransform] = [
                ColorJitterFixed(brightness=0.5,
                                 contrast=0.5),
                RandomHorizontalFlipFixed(p=0.5),
                RandomAffineFixed(degrees=30,
                                  # translate=(.1, .1),
                                  scale=(1.0, 1.25),
                                  interpolation=InterpolationMode.BILINEAR
                                  )
            ]

    def default_seed(self) -> int:
        """Transform the video name to a seed value"""
        return int.from_bytes(str.encode(self.name), byteorder="big")  # str to byte to int

    def to_array(self, nr_frames: int = None, sec_per_frame: float = None,
                 seed: int = None) -> np.ndarray:
        """
        Cast the video frames to a large 4D numpy array (height x width x color x frames). Can also downsample the
        image to a desired time per frame and select a number of frames to include in the array.
        :param nr_frames: Select the number of frames to include in the array. If not given, all (possibly downscaled)
            frames are used.
        :param sec_per_frame: Specify a desired time per frame in the output. The output is downsampled to this number.
            If not given, the regular framerate is used.
        :param seed: Seed used to apply the transformations. Transformations are only applied if a seed is given.
        :return: 4D numpy array with size (nr_frames x RGB channels x height x width) = (nr_frames x 3 x 96 x 96)
        """

        print("video loading data:", nr_frames, sec_per_frame)

        if nr_frames is None:
            nr_frames = self.cvvideo.get(
                CAP_PROP_FRAME_COUNT)  # Set nr_frames to the total amount of frames in the video
        if sec_per_frame is None:
            sec_per_frame = 1 / self.framerate  # Set the target sec per frame based on original framerate

        downsampled_framerate = ceil(self.framerate * sec_per_frame)
        # print("downsampled framerate: ", downsampled_framerate)
        # video_array = np.empty((self.height, self.width, 3, nr_frames))  # 3 channels (rgb)

        # array_frame_idx = 0  # Index of which frame we are currently accessing
        frame_list: List[np.ndarray] = []

        comp = None  # Composition of transformations
        to_pil_image = ToPILImage()
        to_tensor = ToTensor()
        if seed is not None:  # Set the seed if it is given
            random.seed(seed)
            torch.manual_seed(seed)

            for t in self.transforms:  # Fix all random transforms using the seed
                t.fix_transform()

            comp = Compose(self.transforms)  # Create the compose from the fixed transforms

        # Iterate the number of frames to
        for i in range(downsampled_framerate * nr_frames):
            if i % downsampled_framerate == 0:  # Only select some frames, this downsamples the video
                ret, frame = self.cvvideo.read()  # Read the next frame
                # print("read frame:", i)
                try:
                    frame = cvtColor(frame, COLOR_BGR2RGB)  # Convert to RGB first
                except Exception as e:
                    print(e)
                    return np.empty(1)
                # plt.imshow(frame, interpolation='nearest')  # Display face
                # plt.show()

                # Apply transformation
                frame_transformed = frame
                if comp is not None:  # If the random transformations were set because a seed was given, apply it
                    # Convert array to PIL image, apply transformation compose, convert back to tensor and to np array
                    # start = time.time()
                    pil = to_pil_image(frame)
                    pil_transformed = comp(pil)
                    tensor = to_tensor(pil_transformed)
                    frame_transformed = tensor.numpy()
                    frame_transformed = np.moveaxis(frame_transformed, 0, -1)  # Columns from (C x W x H) to (W x H x C)
                    # end = time.time()
                    # print("Transform time:", end-start)

                if frame_transformed.dtype.name == 'float32':  # If the data type is float, convert to uint8 first
                    frame_transformed = (frame_transformed * 255).astype("uint8")

                # plt.imshow(frame_transformed, interpolation='nearest')  # Display face
                # plt.show()
                # start = time.time()
                if DISABLE_DLIB is False:  # If DLIB is enabled
                    face_coordinates = detect_face(frame_transformed, 2)  # Detect face in this frame
                    # end = time.time()
                    # print("detect face time:", end-start)
                    while face_coordinates is None:
                        # if no face can be found, keep trying on next frames
                        print("No face detected!!")
                        return np.empty(1)  # Return empty array
                        # ret, frame = self.cvvideo.read()
                        # if ret is False:
                        #     return np.empty(1)
                        # face_coordinates = detect_face(frame, 2)

                    # Select only the face from the frame
                    face = frame_transformed[face_coordinates[0][1]:face_coordinates[1][1],
                           face_coordinates[0][0]:face_coordinates[1][0], :]
                else:  # DLIB is disabled (for testing only)
                    face = frame_transformed

                # Resize to 96x96 pixels and normalize. If preprocessing fails, also return empty tensor
                try:
                    array_preprocessed = _pre_process_input_image(face)
                except Exception as e:
                    print(e)
                    return np.empty(1)

                # Ensure frame is of correct size (1 x RGB channels x 96 x 96)
                assert array_preprocessed.shape == (1, 3) + ESR.INPUT_IMAGE_SIZE, "incorrect frame size"

                # array_np: np.ndarray = array_preprocessed.numpy()
                # array_moved = np.moveaxis(array_np[0, :, :, :], 0, -1)  # From (1 x C x W x H) to (W x H x C)
                # plt.imshow(array_moved, interpolation='nearest')  # Display face
                # plt.show()

                frame_list.append(array_preprocessed)  # Add to frame list

            else:  # Frame is skipped because of downsampling
                self.cvvideo.grab()  # select next frame but do not read it (faster than actually reading)

        video_array = np.concatenate(frame_list, axis=0)  # Concatenate all frames into a large array

        # Ensure the output is of correct size (nr_frames x RGB channels x 96 x 96)
        assert video_array.shape == (nr_frames, 3) + ESR.INPUT_IMAGE_SIZE

        # Show first frame of video array
        # frame0 = video_array[0, :, :, :]
        # frame0_moved = np.moveaxis(frame0, 0, -1)  # Move the axes from (C x W x H) to (W x H x C)
        # plt.imshow(frame0_moved, interpolation='nearest')  # Display frame
        # plt.show()

        return video_array

    @classmethod
    def triplet_to_array(cls, triplet: Tuple['Video', 'Video', 'Video'], nr_frames: int = None,
                         sec_per_frame: float = None, seed: int = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        Convert a triplet of videos to a possibly downsampled array.
        :param triplet: Triplet of Video objects
        :param nr_frames: Nr of frames (possibly downsampled) to include in the array.
        :param sec_per_frame: Downsample video to have this amount of seconds per frame.
        :param seed: Seed used to apply the transformations. Transformations are only applied if a seed is given.
        :return: A triplet of numpy arrays
        """
        # noinspection PyTypeChecker
        return tuple(
            [video.to_array(nr_frames=nr_frames, sec_per_frame=sec_per_frame, seed=seed) for
             video in triplet])


class VideoLoader:
    DATA_BASE_FOLDER_NAME = 'data'  # Folder name containing the dataset
    DATA_BASE_PATH = Path('data/')

    # DATA_BASE_FOLDER_NAME = 'dataset'  # Folder name containing the dataset
    # DATA_BASE_PATH = Path(__file__).parents[3].absolute() / DATA_BASE_FOLDER_NAME  # Path relative to this script

    def __init__(self, train: bool = True, nr_frames: int = 20, sec_per_frame: float = 0.1, seed: int = None,
                 anchor_positive_fraction: float = 0.5, other_negative_fraction: float = 0.5,
                 hard_triplets: bool = False):
        """
        Initialize the VideoLoader helper class

        :param train: Whether the training set should be used
        :param nr_frames: Nr of frames (possibly downsampled) to include in the array.
        :param sec_per_frame: Downsample video to have this amount of seconds per frame.
        :param seed: Seed used to apply the transformations. Transformations are only applied if a seed is given (not
        None). Seed also influences which sequences are selected to use a positive anchor.
        :param anchor_positive_fraction: What percentage of positive videos of triplets should be the anchor video.
        :param other_negative_fraction: What percentage of negative videos of triplets should be original other video.
        :param hard_triplets: Flag which overrides anchor_positive_fraction and other_negative_fraction to be 0, which
        disables all original sequences as positive and negative.
        """
        # Original paths
        self.youtube: Path = self.DATA_BASE_PATH / "original_sequences" / "youtube" / "c23" / "videos"
        # self.actors: Path = self.DATA_BASE_PATH / "original_sequences" / "actors" / "c23" / "videos"

        # Manipulated paths (fake videos)
        # self.DeepFakeDetection: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "DeepFakeDetection" / "c23" / "videos"
        # self.Deepfakes: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "Deepfakes" / "c23" / "videos"
        self.Face2Face: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "Face2Face" / "c23" / "videos"
        # self.FaceShifter: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "FaceShifter" / "c23" / "videos"
        # self.FaceSwap: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "FaceSwap" / "c23" / "videos"
        self.NeuralTextures: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "NeuralTextures" / "c23" / "videos"

        # Take any manipulated folder to get the pairs of videos (which are all the same for each manipulation type)
        # noinspection PyTypeChecker
        _, filenames = zip(*list(map(os.path.split, list(self.get_source('NeuralTextures').glob('*.mp4')))))
        pairs = list(map(lambda x: (x[0:3], x[4:7]), filenames))
        for pair in pairs:
            pairs.remove((pair[1], pair[0]))  # remove the duplicate (switched) pairs

        # if hard triplets, then only deepfaked videos are used
        self.hard_triplets = hard_triplets

        # Set the randomization seed
        self.seed: Optional[int] = seed

        self.sec_per_frame = sec_per_frame
        self.nr_frames = nr_frames

        # 75% train data, the rest test
        split_train_data = round(len(pairs) * 0.75)

        if train:
            self.videos = [item for sublist in pairs[:split_train_data] for item in sublist]
        elif not train:
            self.videos = [item for sublist in pairs[split_train_data:] for item in sublist]

        # If hard triplets is true, select no original sequences as positive or negative
        if hard_triplets is True:
            anchor_positive_fraction = 1
            other_negative_fraction = 0
            print(f"hard_triplets: {hard_triplets}")

        if not (0 <= anchor_positive_fraction <= 1) or not (0 <= other_negative_fraction <= 1):
            raise ValueError(f"anchor_positive_fraction ({anchor_positive_fraction}) and other_negative_fraction "
                             f"({other_negative_fraction}) should be in range [0, 1]")

        # Set fraction of the videos to have anchor as positive and fraction to have original other as negative
        nr_videos = len(self.videos)
        nr_anchor_positive = round(nr_videos * anchor_positive_fraction)
        nr_other_negative = round(nr_videos * other_negative_fraction)
        print(f"Anchor as positive: {nr_anchor_positive}/{nr_videos} videos")
        print(f"Other as negative : {nr_other_negative}/{nr_videos} videos")

        self.anchor_is_positive = [True] * nr_anchor_positive + [False] * (nr_videos - nr_anchor_positive)
        self.other_is_negative = [True] * nr_other_negative + [False] * (nr_videos - nr_other_negative)

        if self.seed is not None:  # Set the seed if it is given
            random.seed(self.seed)
        random.shuffle(self.anchor_is_positive)  # Shuffle the order of anchors as positive, independent of others
        random.shuffle(self.other_is_negative)  # Shuffle the order of others as negative, independent of anchors

        random.shuffle(self.videos)

    def __getitem__(self, index) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        # Triplets always have the form [Anchor, Positive, Negative]
        while 1:
            if self.hard_triplets:
                anchor_positive = False
                other_negative = False
            else:
                anchor_positive = self.anchor_is_positive[index]
                other_negative = self.other_is_negative[index]

            triplet = self.load_triplet(anchor_source_dir="youtube",  # Original source folder
                                        anchor_filename=self.videos[index],  # Original video name
                                        manipulated_source="NeuralTextures",  # Manipulation technique source folder
                                        anchor_positive=anchor_positive,  # Whether to use original anchor as positive
                                        other_negative=other_negative,  # Whether to use original other as negative
                                        )
            print("Video name:", self.videos[index])

            video_seed: Optional[int] = None  # If this is set to an integer, random transforms are enabled
            if self.seed is not None:  # Only if VideoLoader.seed is set, transform the video
                video_seed = int(triplet[0].name) + self.seed  # Convert the video name to integer and add current seed
            # print(f"Using seed {video_seed}")

            # Convert all elements in the triplet to arrays with the same parameters
            triplet_arrays = Video.triplet_to_array(triplet=triplet,  # Triplet to convert
                                                    nr_frames=self.nr_frames,  # Nr of frames to put in the output
                                                    sec_per_frame=self.sec_per_frame,  # Target seconds per frame
                                                    seed=video_seed  # Seed value for random transforms
                                                    )
            # end = time.time()
            # print("Trip to array:", end - start)
            anchor, pos, neg = triplet_arrays
            if anchor.ndim == 1 or pos.ndim == 1 or neg.ndim == 1:
                print("Empty frames, skipping to other video")
                if index + 1 < len(self.videos):
                    index += 1
                    continue
                else:
                    index = 0
                    continue

            else:
                break
        return triplet_arrays

    def __len__(self) -> int:
        return len(self.videos)

    def get_source(self, source: str) -> Path:
        """Load the path to a data source folder"""
        try:
            return getattr(self, source)
        except AttributeError:
            raise IOError(f"Source [{source}] does not exist")

    def nr_videos(self, source: str) -> int:
        """Return the number of videos in a source list"""
        return len(list(self.get_source(source).glob('*.mp4')))

    def load_video(self, source: str, filename: str = None, index: int = None) -> Video:
        """
        Load specified video in a source folder
        :param source: Source folder to select
        :param filename: File name to select
        :param index: Index to select
        """
        if filename:  # If a filename was provided, use that
            if not Path(filename).suffix:  # If no extension is given, use .mp4
                filename = filename + ".mp4"
            path = self.get_source(source) / filename

        elif index is not None:  # If no filename was given, but an index was given, give the ith element of the list
            files_in_folder = list(self.get_source(source).glob('*.mp4'))  # Get list of all files
            path = files_in_folder[index]

        else:  # Both were not given, but they are required
            raise IOError("Provide a filename or a file number")

        return Video(path)  # Load the video

    def load_triplet(self, anchor_source_dir: str, anchor_filename: str, manipulated_source: str,
                     anchor_positive: bool = False, other_negative: bool = False) -> Tuple[Video, Video, Video]:
        """
        Load a triplet given a source folder and an anchor filename
        :param anchor_source_dir: Source folder which contains anchor video
        :param anchor_filename: Filename of the anchor video (without extension)
        :param manipulated_source: Source folder which contains the fake videos.
        :param anchor_positive: Whether to set the anchor video as positive, with a time offset so that it starts
        halfway through the video
        :param other_negative: Whether to set the original other video as negative, with a time offset so that it
        starts halfway through the video
        :return: Tuple of three Video objects: [Anchor, Positive, Negative]. Anchor is the original video. Positive is
            the video with the anchor's facial expression. Negative is the video the other's facial expression.
        """
        manipulated_filenames = self.get_source(manipulated_source).glob('*.mp4')  # Get all filenames of fakes

        # Get all filenames containing the anchor name. Based on https://stackoverflow.com/a/4843172
        manipulated_files_with_anchor = [file for file in manipulated_filenames if anchor_filename in str(file.stem)]

        if len(manipulated_files_with_anchor) is not 2:  # Check that there are exactly 2 fake videos with this anchor
            raise IOError(f"There must be exactly 2 manipulated videos which contain the anchor name, "
                          f"found [{len(manipulated_files_with_anchor)}]. "
                          f"Files found: {[file.stem for file in manipulated_files_with_anchor]}")

        # All filenames are of the form <target sequence>_<source sequence>. Source is the video from which the facial
        # expressions are obtained. Target is the video with a different background and face in which the facial
        # expressions are placed.
        # This very ugly statement checks which of the two found manipulated videos has the anchor as source
        # Source: https://github.com/ondyari/FaceForensics/blob/master/dataset/README.md#manipulated-sequence-filenames
        if manipulated_files_with_anchor[0].stem.split("_")[1] == anchor_filename:  # First found is "<other>_<anchor>"
            mimicry_of_anchor_file = manipulated_files_with_anchor[0]  # "<other>_<anchor>"
            mimicry_of_other_file = manipulated_files_with_anchor[1]  # "<anchor>_<other>"
            other_filename = manipulated_files_with_anchor[0].stem.split("_")[0]
        else:  # Second video found is "<other>_<anchor>"
            mimicry_of_anchor_file = manipulated_files_with_anchor[1]  # "<other>_<anchor>"
            mimicry_of_other_file = manipulated_files_with_anchor[0]  # "<anchor>_<other>"
            other_filename = manipulated_files_with_anchor[1].stem.split("_")[0]

        print(f"Anchor video name: {anchor_filename}, Other video name: {other_filename}")
        # print(f"Anchor as source : {mimicry_of_anchor_file.stem}, other as source: {mimicry_of_other_file.stem}")
        # print(f"Anchor as positive: {anchor_positive}, Other as negative: {other_negative}")

        # Load the anchor video (always original sequence)
        anchor_vid = self.load_video(source=anchor_source_dir, filename=anchor_filename)  # Anchor video

        triplet_string = f"Anchor: {anchor_filename}, "

        # Select the positive video
        if anchor_positive:  # Set the original anchor video to be the positive instead of a manipulated sequence
            positive_vid = self.load_video(source=anchor_source_dir, filename=anchor_filename)  # Copy the anchor
            halfway_frame = positive_vid.cvvideo.get(CAP_PROP_FRAME_COUNT) // 2  # Integer divide total nr of frames
            positive_vid.cvvideo.set(CAP_PROP_POS_FRAMES, halfway_frame)  # Set the starting frame to halfway
            # positive is now the anchor image, anchor mimicry, looks like anchor
            triplet_string += f"Positive: {anchor_filename}, "
        else:  # Use a manipulated sequence as positive
            positive_vid = Video(path=mimicry_of_anchor_file)  # Anchor mimicry, looks like other
            triplet_string += f"Positive: {mimicry_of_anchor_file.stem}, "

        # Select the negative video
        if other_negative:  # Set the original other video to be the negative instead of a manipulated sequence
            negative_vid = self.load_video(source=anchor_source_dir, filename=other_filename)  # Copy the anchor
            # Negative is now the other image, other mimicry, looks like other
            triplet_string += f"Negative: {other_filename}, "
        else:  # Use a manipulated sequence as negative
            negative_vid = Video(mimicry_of_other_file)  # Other mimicry, looks like anchor
            triplet_string += f"Negative: {mimicry_of_other_file.stem}, "

        print(triplet_string)

        return anchor_vid, positive_vid, negative_vid  # Return video objects


if __name__ == '__main__':
    loader = VideoLoader(train=True)

    # Load a video
    vid_000: Video = loader.load_video(source="youtube", filename="000")

    # Get an array from te video after downsampling the frames to 10 fps
    # downsampled_arr = vid_000.to_array(nr_frames=3, sec_per_frame=1 / 10)

    # Get the triplet corresponding to an original image
    # Triplets always have the form [Anchor, Positive, Negative]
    triplet_000 = loader.load_triplet(anchor_source_dir="youtube",  # Original source folder
                                      anchor_filename="003",  # Original video name
                                      manipulated_source="NeuralTextures",  # Manipulation technique source folder
                                      anchor_positive=True)  # Whether to use the anchor as a positive

    # import sys
    #
    # sys.exit(0)

    # Convert all elements in the triplet to arrays with the same parameters
    triplet_000_arrays = Video.triplet_to_array(triplet=triplet_000,  # Triplet to convert
                                                nr_frames=10,  # Nr of frames to put in the output
                                                sec_per_frame=1 / 10,  # Target seconds per frame
                                                seed=3)

    # Alternative way to get video triplets to arrays with different parameters
    # triplet_000_arrays2: List[np.ndarray] = []
    # triplet_000_arrays2.append(triplet_000[0].to_array(nr_frames=3, sec_per_frame=1 / 10))
    # triplet_000_arrays2.append(triplet_000[1].to_array(nr_frames=3, sec_per_frame=1 / 10))

    anchor_array = triplet_000_arrays[0]  # Select anchor array
    anchor_frame1 = anchor_array[1, :, :, :]  # Select first frame
    anchor_frame1_moved = np.moveaxis(anchor_frame1, 0, -1)  # Move the axes from (C x W x H) to (W x H x C)
    anchor_frame1_rgb = cvtColor(anchor_frame1_moved, COLOR_BGR2RGB)  # Convert from BGR to RGB

    # Display the frame
    plt.imshow(anchor_frame1_rgb, interpolation='nearest')
    plt.show()
