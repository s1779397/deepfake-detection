import os
import os.path

import numpy as np
import torch
import cv2
from math import ceil
import controller.cvision as cvision
import model.utils.uimage
import matplotlib.pyplot as plt

INPUT_IMAGE_SIZE = (96, 96)

def default_video_loader(path, no_images_per_video, sec_per_image):
    video = cv2.VideoCapture(path)
    fps = video.get(cv2.CAP_PROP_FPS)
    # width = round(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    # height = round(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    frames_per_image = ceil(fps * sec_per_image)
    # video_array = np.empty((no_images_per_video, 3, INPUT_IMAGE_SIZE[0], INPUT_IMAGE_SIZE[0]))  # 3 channels (rgb)
    face_list = []

    # j = 0
    for i in range(frames_per_image * no_images_per_video):
        if i % frames_per_image == 0:
            ret, frame = video.read()

            # detect and cut out face
            face_coordinates = cvision.detect_face(frame, 2)
            while face_coordinates is None:
                # if no face can be found, keep trying on next frames
                print("No face detected!!")
                ret, frame = video.read()
                face_coordinates = cvision.detect_face(frame)

            print("face detected!")
            face = frame[face_coordinates[0][1]:face_coordinates[1][1],
                         face_coordinates[0][0]:face_coordinates[1][0], :]

            # preprocess face
            face = cvision._pre_process_input_image(face).numpy()
            face_list.append(face)

            # face = np.moveaxis(face, 2, 0)
            # video_array[j, :, :, :] = face
            # j += 1
        video.grab()

    video_array = np.concatenate(face_list)
    print(video_array.shape)
    return video_array


class TripletImageLoader(torch.utils.data.Dataset):
    def __init__(self, base_path, filenames_filename, triplets_file_name, no_images_per_video, sec_per_image, transform=None,
                 loader=default_video_loader):
        """ filenames_filename: A text file with each line containing the path to an image e.g.,
                images/class1/sample.jpg
            triplets_file_name: A text file with each line containing three integers,
                where integer i refers to the i-th image in the filenames file.
                For a line of intergers 'a b c', a triplet is defined such that image a is more
                similar to image c than it is to image b, e.g.,
                0 2017 42 """
        self.base_path = base_path
        self.filenamelist = []
        for line in open(filenames_filename):
            self.filenamelist.append(line.rstrip('\n'))
        triplets = []
        for line in open(triplets_file_name):
            triplets.append((line.split()[0], line.split()[1], line.split()[2])) # anchor, far, close
        self.triplets = triplets
        self.transform = transform
        self.loader = loader
        self.no_images_per_video = no_images_per_video
        self.sec_per_image = sec_per_image

    def __getitem__(self, index):
        path1, path2, path3 = self.triplets[index]
        img1 = self.loader(os.path.join(self.base_path, self.filenamelist[int(path1)]),
                           self.no_images_per_video, self.sec_per_image)
        img2 = self.loader(os.path.join(self.base_path, self.filenamelist[int(path2)]),
                           self.no_images_per_video, self.sec_per_image)
        img3 = self.loader(os.path.join(self.base_path, self.filenamelist[int(path3)]),
                           self.no_images_per_video, self.sec_per_image)
        if self.transform is not None:
            img1 = self.transform(img1)
            img2 = self.transform(img2)
            img3 = self.transform(img3)

        return img1, img2, img3

    def __len__(self):
        return len(self.triplets)


def get_data():
    no_images_per_video = 2
    sec_per_image = 0.04167  # 24 fps
    data = TripletImageLoader('data/original_sequences/youtube/c23/videos', 'model/utils/filenames.txt',
                              'model/utils/triplet_filenames.txt',
                              no_images_per_video, sec_per_image)
    return data

