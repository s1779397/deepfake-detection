from pathlib import Path
from typing import Tuple, List, Optional
from math import ceil
import os

import PIL.Image
import numpy as np
from matplotlib import pyplot as plt
from cv2 import VideoCapture, CAP_PROP_FPS, CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_HEIGHT, cvtColor, COLOR_BGR2RGB, \
    CAP_PROP_POS_FRAMES, CAP_PROP_FRAME_COUNT
from torchvision.transforms import Compose, ToPILImage, ToTensor
from model.ml.esr_9 import ESR
import torch
import random
# import time
# from PIL import Image
# import dlib
from controller.cvision import detect_face, _pre_process_input_image

DISABLE_DLIB = False


class Video:
    """VideoCapture wrapper class to hold more information than cv2.VideoCapture"""

    def __init__(self, path: Path):
        """
        Create new Video object which holds a cv2.VideoCapture and additional information
        :param path: Path to the filename to load
        :param transforms: List of transforms to apply. If not given, some defaults are used.
        """
        self.cvvideo = VideoCapture(str(path))
        self.framerate: float = self.cvvideo.get(CAP_PROP_FPS)
        self.width: int = round(self.cvvideo.get(CAP_PROP_FRAME_WIDTH))
        self.height: int = round(self.cvvideo.get(CAP_PROP_FRAME_HEIGHT))
        self.name: str = path.stem  # Only the filename without extension


    def default_seed(self) -> int:
        """Transform the video name to a seed value"""
        return int.from_bytes(str.encode(self.name), byteorder="big")  # str to byte to int

    def to_array(self, nr_frames: int = None, sec_per_frame: float = None,
                 seed: int = None) -> np.ndarray:
        """
        Cast the video frames to a large 4D numpy array (height x width x color x frames). Can also downsample the
        image to a desired time per frame and select a number of frames to include in the array.
        :param nr_frames: Select the number of frames to include in the array. If not given, all (possibly downscaled)
            frames are used.
        :param sec_per_frame: Specify a desired time per frame in the output. The output is downsampled to this number.
            If not given, the regular framerate is used.
        :param seed: Seed used to apply the transformations. Transformations are only applied if a seed is given.
        :return: 4D numpy array with size (nr_frames x RGB channels x height x width) = (nr_frames x 3 x 96 x 96)
        """

        print("video loading data:", nr_frames, sec_per_frame)

        if nr_frames is None:
            nr_frames = self.cvvideo.get(
                CAP_PROP_FRAME_COUNT)  # Set nr_frames to the total amount of frames in the video
        if sec_per_frame is None:
            sec_per_frame = 1 / self.framerate  # Set the target sec per frame based on original framerate

        downsampled_framerate = ceil(self.framerate * sec_per_frame)
        # print("downsampled framerate: ", downsampled_framerate)
        # video_array = np.empty((self.height, self.width, 3, nr_frames))  # 3 channels (rgb)

        # array_frame_idx = 0  # Index of which frame we are currently accessing
        frame_list: List[np.ndarray] = []

        comp = None  # Composition of transformations
        to_pil_image = ToPILImage()
        to_tensor = ToTensor()

        # Iterate the number of frames to
        for i in range(downsampled_framerate * nr_frames):
            if i % downsampled_framerate == 0:  # Only select some frames, this downsamples the video
                ret, frame = self.cvvideo.read()  # Read the next frame
                # print("read frame:", i)
                try:
                    frame = cvtColor(frame, COLOR_BGR2RGB)  # Convert to RGB first
                except Exception as e:
                    print(e)
                    return np.empty(1)
                # plt.imshow(frame, interpolation='nearest')  # Display face
                # plt.show()

                # Apply transformation
                frame_transformed = frame

                if frame_transformed.dtype.name == 'float32':  # If the data type is float, convert to uint8 first
                    frame_transformed = (frame_transformed * 255).astype("uint8")

                # plt.imshow(frame_transformed, interpolation='nearest')  # Display face
                # plt.show()
                # start = time.time()
                if DISABLE_DLIB is False:  # If DLIB is enabled
                    face_coordinates = detect_face(frame_transformed, 2)  # Detect face in this frame
                    # end = time.time()
                    # print("detect face time:", end-start)
                    while face_coordinates is None:
                        # if no face can be found, keep trying on next frames
                        print("No face detected!!")
                        return np.empty(1)  # Return empty array
                        # ret, frame = self.cvvideo.read()
                        # if ret is False:
                        #     return np.empty(1)
                        # face_coordinates = detect_face(frame, 2)

                    # Select only the face from the frame
                    face = frame_transformed[face_coordinates[0][1]:face_coordinates[1][1],
                           face_coordinates[0][0]:face_coordinates[1][0], :]
                else:  # DLIB is disabled (for testing only)
                    face = frame_transformed

                # Resize to 96x96 pixels and normalize. If preprocessing fails, also return empty tensor
                try:
                    array_preprocessed = _pre_process_input_image(face)
                except Exception as e:
                    print(e)
                    return np.empty(1)

                # Ensure frame is of correct size (1 x RGB channels x 96 x 96)
                assert array_preprocessed.shape == (1, 3) + ESR.INPUT_IMAGE_SIZE, "incorrect frame size"

                # array_np: np.ndarray = array_preprocessed.numpy()
                # array_moved = np.moveaxis(array_np[0, :, :, :], 0, -1)  # From (1 x C x W x H) to (W x H x C)
                # plt.imshow(array_moved, interpolation='nearest')  # Display face
                # plt.show()

                frame_list.append(array_preprocessed)  # Add to frame list

            else:  # Frame is skipped because of downsampling
                self.cvvideo.grab()  # select next frame but do not read it (faster than actually reading)

        video_array = np.concatenate(frame_list, axis=0)  # Concatenate all frames into a large array

        # Ensure the output is of correct size (nr_frames x RGB channels x 96 x 96)
        assert video_array.shape == (nr_frames, 3) + ESR.INPUT_IMAGE_SIZE

        # Show first frame of video array
        # frame0 = video_array[0, :, :, :]
        # frame0_moved = np.moveaxis(frame0, 0, -1)  # Move the axes from (C x W x H) to (W x H x C)
        # plt.imshow(frame0_moved, interpolation='nearest')  # Display frame
        # plt.show()

        return video_array


class SingleVideoLoader:
    DATA_BASE_FOLDER_NAME = 'data'  # Folder name containing the dataset
    DATA_BASE_PATH = Path('data/')

    # DATA_BASE_FOLDER_NAME = 'dataset'  # Folder name containing the dataset
    # DATA_BASE_PATH = Path(__file__).parents[3].absolute() / DATA_BASE_FOLDER_NAME  # Path relative to this script

    def __init__(self, train: bool = True, nr_frames: int = 20, sec_per_frame: float = 0.1):
        """
        Initialize the VideoLoader helper class

        :param train: Whether the training set should be used
        :param nr_frames: Nr of frames (possibly downsampled) to include in the array.
        :param sec_per_frame: Downsample video to have this amount of seconds per frame.
        :param seed: Seed used to apply the transformations. Transformations are only applied if a seed is given (not
        None). Seed also influences which sequences are selected to use a positive anchor.
        :param anchor_positive_fraction: What percentage of positive videos of triplets should be the anchor video.
        :param other_negative_fraction: What percentage of negative videos of triplets should be original other video.
        :param hard_triplets: Flag which overrides anchor_positive_fraction and other_negative_fraction to be 0, which
        disables all original sequences as positive and negative.
        """
        # Original paths
        self.youtube: Path = self.DATA_BASE_PATH / "original_sequences" / "youtube" / "c23" / "videos"
        # self.actors: Path = self.DATA_BASE_PATH / "original_sequences" / "actors" / "c23" / "videos"

        # Manipulated paths (fake videos)
        # self.DeepFakeDetection: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "DeepFakeDetection" / "c23" / "videos"
        # self.Deepfakes: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "Deepfakes" / "c23" / "videos"
        self.Face2Face: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "Face2Face" / "c23" / "videos"
        # self.FaceShifter: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "FaceShifter" / "c23" / "videos"
        # self.FaceSwap: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "FaceSwap" / "c23" / "videos"
        self.NeuralTextures: Path = self.DATA_BASE_PATH / "manipulated_sequences" / "NeuralTextures" / "c23" / "videos"

        # Take any manipulated folder to get the pairs of videos (which are all the same for each manipulation type)
        # noinspection PyTypeChecker
        _, filenames = zip(*list(map(os.path.split, list(self.get_source('NeuralTextures').glob('*.mp4')))))
        pairs = list(map(lambda x: (x[0:3], x[4:7]), filenames))
        for pair in pairs:
            pairs.remove((pair[1], pair[0]))  # remove the duplicate (switched) pairs

        # 75% train data, the rest test
        split_train_data = round(len(pairs) * 0.75)

        if train:
            self.videos = [item for sublist in pairs[:split_train_data] for item in sublist]
        elif not train:
            self.videos = [item for sublist in pairs[split_train_data:] for item in sublist]

        self.sec_per_frame = sec_per_frame
        self.nr_frames = nr_frames

    def __getitem__(self, index):
        # Triplets always have the form [Anchor, Positive, Negative]
        triplet, filenames = self.load_videos(anchor_source_dir="youtube",  # Original source folder
                                              anchor_filename=self.videos[index],  # Original video name
                                              manipulated_source="NeuralTextures",
                                              # Manipulation technique source folder
                                              )

        # Convert all elements in the triplet to arrays with the same parameters
        triplet_arrays = [video.to_array(nr_frames=self.nr_frames, sec_per_frame=self.sec_per_frame)
                          for video in triplet]

        return triplet_arrays, filenames

    def __len__(self) -> int:
        return len(self.videos)

    def get_source(self, source: str) -> Path:
        """Load the path to a data source folder"""
        try:
            return getattr(self, source)
        except AttributeError:
            raise IOError(f"Source [{source}] does not exist")

    def nr_videos(self, source: str) -> int:
        """Return the number of videos in a source list"""
        return len(list(self.get_source(source).glob('*.mp4')))

    def load_video(self, source: str, filename: str = None, index: int = None) -> Video:
        """
        Load specified video in a source folder
        :param source: Source folder to select
        :param filename: File name to select
        :param index: Index to select
        """
        if filename:  # If a filename was provided, use that
            if not Path(filename).suffix:  # If no extension is given, use .mp4
                filename = filename + ".mp4"
            path = self.get_source(source) / filename

        elif index is not None:  # If no filename was given, but an index was given, give the ith element of the list
            files_in_folder = list(self.get_source(source).glob('*.mp4'))  # Get list of all files
            path = files_in_folder[index]

        else:  # Both were not given, but they are required
            raise IOError("Provide a filename or a file number")

        return Video(path)  # Load the video

    def load_videos(self, anchor_source_dir: str, anchor_filename: str, manipulated_source: str):
        """
        Load a triplet given a source folder and an anchor filename
        :param anchor_source_dir: Source folder which contains anchor video
        :param anchor_filename: Filename of the anchor video (without extension)
        :param manipulated_source: Source folder which contains the fake videos.
        :param anchor_positive: Whether to set the anchor video as positive, with a time offset so that it starts
        halfway through the video
        :param other_negative: Whether to set the original other video as negative, with a time offset so that it
        starts halfway through the video
        :return: Tuple of three Video objects: [Anchor, Positive, Negative]. Anchor is the original video. Positive is
            the video with the anchor's facial expression. Negative is the video the other's facial expression.
        """
        manipulated_filenames = self.get_source(manipulated_source).glob('*.mp4')  # Get all filenames of fakes

        # Get all filenames containing the anchor name. Based on https://stackoverflow.com/a/4843172
        manipulated_files_with_anchor = [file for file in manipulated_filenames if
                                         anchor_filename + '_' in str(file.stem)]

        if len(manipulated_files_with_anchor) is not 1:  # Check that there are exactly 1 fake videos with this anchor
            raise IOError(f"There must be exactly 1 manipulated videos which contain the anchor name + _, "
                          f"found [{len(manipulated_files_with_anchor)}]. "
                          f"Files found: {[file.stem for file in manipulated_files_with_anchor]}")

        negative = manipulated_files_with_anchor[0]

        # Load the anchor video (always original sequence)
        anchor_vid = self.load_video(source=anchor_source_dir, filename=anchor_filename)  # Anchor video

        positive_vid = self.load_video(source=anchor_source_dir, filename=anchor_filename)  # Copy the anchor
        halfway_frame = positive_vid.cvvideo.get(CAP_PROP_FRAME_COUNT) // 2  # Integer divide total nr of frames
        positive_vid.cvvideo.set(CAP_PROP_POS_FRAMES, halfway_frame)  # Set the starting frame to halfway

        negative_vid = Video(negative)  # Other mimicry, looks like anchor
        negative_filename = os.path.split(negative)[1]

        return (anchor_vid, positive_vid, negative_vid), \
               (anchor_filename, anchor_filename + "_offset", negative_filename)  # Return video objects
