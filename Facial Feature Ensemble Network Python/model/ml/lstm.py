# Source: https://towardsdatascience.com/building-a-lstm-by-hand-on-pytorch-59c02a4ec091
import math
import torch
import torch.nn as nn
from model.ml.esr_9 import ESR
import numpy as np


class CustomLSTM(nn.Module):

    INPUT_TENSOR_SIZE = 512
    HIDDEN_TENSOR_SIZE = 512

    def __init__(self, device):
        super().__init__()
        self.input_sz = self.INPUT_TENSOR_SIZE
        self.hidden_size = self.HIDDEN_TENSOR_SIZE
        self.W = nn.Parameter(torch.Tensor(self.input_sz, self.hidden_size * 4))
        self.U = nn.Parameter(torch.Tensor(self.hidden_size, self.hidden_size * 4))
        self.bias = nn.Parameter(torch.Tensor(self.hidden_size * 4))
        self.init_weights()

    def init_weights(self):
        stdv = 1.0 / math.sqrt(self.hidden_size)
        for weight in self.parameters():
            weight.data.uniform_(-stdv, stdv)

    def forward(self, x,
                init_states=None):
        """Assumes x is of shape (batch, sequence, feature)"""
        bs, seq_sz, _ = x.size()
        hidden_seq = []
        if init_states is None:
            h_t, c_t = (torch.zeros(bs, self.hidden_size).to(x.device),
                        torch.zeros(bs, self.hidden_size).to(x.device))
        else:
            h_t, c_t = init_states

        HS = self.hidden_size
        for t in range(seq_sz):
            x_t = x[:, t, :]
            # batch the computations into a single matrix multiplication
            gates = x_t @ self.W + h_t @ self.U + self.bias
            i_t, f_t, g_t, o_t = (
                torch.sigmoid(gates[:, :HS]),  # input
                torch.sigmoid(gates[:, HS:HS * 2]),  # forget
                torch.tanh(gates[:, HS * 2:HS * 3]),
                torch.sigmoid(gates[:, HS * 3:]),  # output
            )
            c_t = f_t * c_t + i_t * g_t
            h_t = o_t * torch.tanh(c_t)
            hidden_seq.append(h_t.unsqueeze(0))
        hidden_seq = torch.cat(hidden_seq, dim=0)
        # reshape from shape (sequence, batch, feature) to (batch, sequence, feature)
        hidden_seq = hidden_seq.transpose(0, 1).contiguous()
        return hidden_seq, (h_t, c_t)


class lstmESR(nn.Module):
    def __init__(self, device):
        super().__init__()

        self.esr = ESR(device)
        self.lstm = CustomLSTM(device)

        self.esr.to(device)
        self.lstm.to(device)
        self.to(device)
        self.global_pool = nn.AdaptiveAvgPool2d(1)

    def forward(self, x, i):
        """Assumes x is of shape (batch, sequence, feature)"""
        batch_sz, seq_sz, _, _, _ = x.size()  # has rgb image input
        # print(x.size())
        x = torch.flatten(x, start_dim=0, end_dim=1)  # flatten batch and sequence for single batch size of batch_sz*feature_sz
        # print(x.size())
        # forward through esr network and single conv branch
        x_base = self.esr.base(x)
        x_features = self.esr.convolutional_branches[i].forward_to_last_conv_layer(x_base)
        x_pool = self.global_pool(x_features)
        # print(x_pool.size())

        lstm_shape = (batch_sz, seq_sz, np.prod(x_pool[0].size()))  # reshape to single vector with sequence and batch
        x_pool = torch.reshape(x_pool, lstm_shape)
        # print(x_pool.size())
        hidden_seq, init_states = self.lstm.forward(x_pool)  # forward reshaped features

        return hidden_seq

