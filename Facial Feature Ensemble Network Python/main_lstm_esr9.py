from controller import cvision
from model.utils import uimage, ufile
import numpy as np

input_image_path_1 = "./media/ferrari.PNG"
input_image_path_2 = "./media/ferrari_2.PNG"


def main():
    img_1 = uimage.read(input_image_path_1)
    img_2 = uimage.read(input_image_path_2)
    gpu = False
    lstm_out = cvision.get_lstm_features(np.stack((img_1, img_2)), gpu)
    print(lstm_out)
    print(lstm_out.size())
    print(lstm_out[0][0] - lstm_out[0][1])


main()
