import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from torch.utils.data import DataLoader
import torch
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import RocCurveDisplay
from pathlib import Path

from model.ml.lstm import lstmESR
from model.utils import single_video_loader

BRANCH = 1
FRAMES = 40
DATASET = 'anchor_pos_b4_f2f'

plt.rcParams.update({'font.size': 11})


def save_embeddings():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)

    net = lstmESR(device)
    net.load_state_dict(torch.load("model/ml/trained_models/esr_lstm/"
                                   + DATASET + "_" + str(FRAMES) + "fr_esr_lstm.pt"))
    net.eval()

    test_data = single_video_loader.SingleVideoLoader(train=False, nr_frames=FRAMES)
    test_loader = DataLoader(test_data, batch_size=1)

    embedding_dict = {'videoName': [],
                      'embedding': []}

    for triplet, filenames in test_loader:
        anchor, pos, neg = triplet
        anchor_name, pos_name, neg_name = filenames

        for name, embedding in [(anchor_name, anchor), (pos_name, pos), (neg_name, neg)]:

            embedding = embedding.to(device)

            if embedding.ndim > 2:
                output = net(embedding, BRANCH).squeeze()[-1]
                embedding_dict['embedding'].append(output.detach().cpu().numpy())
            else:
                output = None
                embedding_dict['embedding'].append(output)

            embedding_dict['videoName'].append(name)


    np.save("results/embeddings/" + str(FRAMES) + "/" + DATASET + ".npy", embedding_dict, allow_pickle=True)


def confirm_deepfake_detection(ax):
    embedding_dict = np.load("results/embeddings/" + str(FRAMES) + "/" + DATASET + ".npy", allow_pickle=True)
    filenames = embedding_dict.item()['videoName']
    embeddings = embedding_dict.item()['embedding']

    true_labels = []
    distances = []

    i = 0
    for embedding, filename in zip(embeddings, filenames):
        #print("Embedding:", embedding)
        if i == 0:
            anchor = embedding
            filename_anchor = filename
        elif i == 1:
            positive = embedding
            filename_pos = filename
        elif i == 2:
            negative = embedding
            filename_neg = filename

            #print("Filename 1:", filename_anchor)
            #print("Filename 2:", filename_pos)
            #print("Filename 3:", filename_neg)
            if (anchor is not None) and (positive is not None):
                # print("Got value for pos")
                dis_to_pos = np.linalg.norm(anchor-positive)
                distances.append(dis_to_pos)
                true_labels.append(1)

            if (anchor is not None) and (negative is not None):
                # print("Got value for neg")
                dis_to_neg = np.linalg.norm(anchor-negative)
                distances.append(dis_to_neg)
                true_labels.append(0)

        i += 1
        i %= 3

    distances = np.array(distances)
    dist_norm = distances / max(distances)
    scores = 1 - dist_norm

    fpr, tpr, threshold = roc_curve(true_labels, scores, pos_label=1)
    fnr = 1 - tpr
    AUC = auc(fpr, tpr)
    EER = fpr[np.nanargmin(np.absolute((fnr - fpr)))]
    print("EER deepfake detection:", EER)
    print("AUC deepfake detection:", AUC)

    #print("Threshold =", threshold)
    #print("tpr =", tpr)
    #print("fpr =", fpr)
    RocCurveDisplay(fpr=fpr, tpr=tpr).plot(ax=ax, linewidth=4)
    # plt.show()


def confirm_expression_use(ax):
    embedding_dict = np.load("results/embeddings/" + str(FRAMES) + "/" + DATASET + ".npy", allow_pickle=True)
    filenames = embedding_dict.item()['videoName']
    embeddings = embedding_dict.item()['embedding']

    true_labels = []
    distances = []

    i = 0
    for embedding, filename in zip(embeddings, filenames):
        if i == 0:
            anchor = embedding
            filename_anchor = filename
        elif i == 2:
            negative = embedding
            filename_neg = filename

            filename_base = Path(filename[0]).stem
            filename_new = filename_base.split('_')[1] + '_' + filename_base.split('_')[0]
            if (filename_new + ".mp4",) in filenames:
                index_pos = filenames.index((filename_new + ".mp4",))
                positive = embeddings[index_pos]
            else:
                positive = None
            filename_pos = filename_new

            #rint("Filename 1:", filename_anchor)
            #print("Filename 2:", filename_pos)
            #print("Filename 3:", filename_neg)

            if (anchor is not None) and (positive is not None):
                dis_to_pos = np.linalg.norm(anchor-positive)
                distances.append(dis_to_pos)
                true_labels.append(1)

            if (anchor is not None) and (negative is not None):
                dis_to_neg = np.linalg.norm(anchor-negative)
                distances.append(dis_to_neg)
                true_labels.append(0)

        i += 1
        i %= 3

    distances = np.array(distances)
    dist_norm = distances / max(distances)
    scores = 1 - dist_norm

    fpr, tpr, threshold = roc_curve(true_labels, scores, pos_label=1)
    fnr = 1 - tpr
    AUC = auc(fpr, tpr)
    EER = fpr[np.nanargmin(np.absolute((fnr - fpr)))]
    print("EER expression use:", EER)
    print("AUC expression use:", AUC)
    #print("Threshold =", threshold)
    #print("tpr =", tpr)
    #print("fpr =", fpr)
    RocCurveDisplay(fpr=fpr, tpr=tpr).plot(ax=ax, linewidth=4)
    # plt.show()


def main():
    print("started")
    save_embeddings()
    print("finished")
    #
    # fig, ax = plt.subplots()
    # confirm_expression_use(ax)
    # confirm_deepfake_detection(ax)
    # plt.legend(['Only deepfakes', 'Deepfake and original'])
    # plt.title('ROC curve for performance of LSTM-FERF (NT, 40 frames)')
    # plt.grid()
    # plt.show()


main()
